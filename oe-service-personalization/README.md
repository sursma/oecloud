# oe-service-personalization

## dependency
* oe-cloud
* oe-logger
* oe-expression
* oe-personalization

## Install and test

```sh
$ git clone https://github.com/EdgeVerve/oe-service-personalization.git
$ cd oe-service-personalization
$ npm install --no-optional
$ # Just run test cases
$ npm run test
$ # Run test cases along with code coverage - code coverage report will be available in coverage folder
$ npm run grunt-cover
```
