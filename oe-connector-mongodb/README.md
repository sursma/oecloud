# loopback-connector-mongodb


This is an extension to loopback-connector-mongodb (https://github.com/strongloop/loopback-connector-mongodb)

This project is changed from original project for
* To query Mongo database to perform Groupby and aggregation operations. This is very specific to Oe-cloud.

**Note**: Test cases may not work.

## Please refer loopback documentation at [Github](https://github.com/strongloop/loopback-connector-mongodb).

